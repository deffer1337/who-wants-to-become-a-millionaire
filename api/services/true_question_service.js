

class TrueQuestionService {
    constructor(trueQuesionRepository) {
        this.trueQuesionRepository = trueQuesionRepository
    }

    async getTrueQuestion(questionId) {
        return new Promise((resolve, _) => {
            resolve(this.trueQuesionRepository.findOne({
                where: {
                    questionId: questionId,
                },
            }));
        });
    }
}

module.exports = TrueQuestionService;
