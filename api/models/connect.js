const {Sequelize} = require('sequelize');

const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: "./api/database.db",
    define: {
        timestamps: false,
        createdAt: false,
        updatedAt: false,
    }
});

module.exports = sequelize;