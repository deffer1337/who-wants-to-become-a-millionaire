## API

### GET /api/users

Запрос всех пользователей

Пример ответа:

```json
[
  {
    "id": 1,
    "username": "test",
    "points": 0
  }
]
```

### GET /api/users/{user_id}

Запрос пользователя по username

Пример ответа:

```json
{
  "id": 0,
  "username": "test",
  "points": 0
}
```

### PUT /api/users/{user_id}

Запрос на обновление пользователя по username

Запрос должен содержать json с обновляемыми данными

Пример запроса:

```json
{
  "points": 1337
}
```

Примеры ответа:

Если все ок:

```json
{
  "message": "Success"
}
```
Если не удалось обновить юзера:

```json
{
  "message": "Not success"
}
```

Если не прошёл валидацию:

```json
{
  "message": "Validation error: Validation isInt on points failed"
}
```



### POST /api/users

Запрос на создание пользователя

Запрос должен содержать json с нужными данными

Пример запроса:

```json
{
  "username": "test"
}
```


Примеры ответа:

Если все ок:
```json
{
  "id": 1,
  "username": "test"
}
```

Если не прошёл валидацию:

```json
{"message": "Validation error: Validation isInt on points failed"}
```

or

```json
{"message":"Validation error: Validation len on username failed"}
```

Иначе:

```json
{
  "message": "Bad request"
}
```

### GET /api/questions/{complexity}

Запрос на получение вопроса со сложностью

Пример ответа:

```json
{
  "id": 108,
  "name": "Что делают зубки у маленьких детей?",
  "A": "режутся",
  "B": "точатся",
  "C": "пилятся",
  "D": "строгаются",
  "complexity": 1
}
```

### GET /api/true_questions/{question_id}

Запрос на получение правильного ответа

Пример ответа:

```json
{
  "id": 108,
  "answer": "A",
  "questionId": 108
}
```