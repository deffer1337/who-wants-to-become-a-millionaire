const UserService = require('./user_service');
const QuestionService = require('./question_service');
const TrueQuestionService = require('./true_question_service');

module.exports = {
    UserService: UserService,
    QuestionService: QuestionService,
    TrueQuestionService: TrueQuestionService,
};
