const {DataTypes} = require('sequelize');
const sequelize = require('./connect');


const user = sequelize.define(
    'user',
    {
        username: {
            type: DataTypes.STRING(20),
            allowNull: false,
            validate: {
                len: [1, 20]
            }
        },
        points: {
            type: DataTypes.INTEGER,
            default: 0,
            validate: {
                isInt: true,
            }
        },
    },
    {
        // Здесь определяются другие настройки модели
    }
);

module.exports = user;
