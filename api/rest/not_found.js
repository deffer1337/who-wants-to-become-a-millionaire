

async function NotFound(request, response){
    response.writeHead(404, {"Content-Type": "application/json"});
    response.end(JSON.stringify({message: "Route not found"}));
}

module.exports = NotFound;
