const BLUE = "#070556";
const ORANGE = "#e1a02e"

let answers = null

window.onload = function() {
    setAnswers();
    setAnswerHandlers();
    setHandlers();
}

function getAnswerElements(id) {
    const answer = document.getElementById(id);
    return {
        button: answer,
        imgFill: answer.children[0].contentDocument.getElementById("fill"),
        numElement: answer.children[1].children[0],
        text: answer.children[1].children[1],
    };
}

function setAnswers() {
    answers = {
        A: getAnswerElements("answer-A"),
        B: getAnswerElements("answer-B"),
        C: getAnswerElements("answer-C"),
        D: getAnswerElements("answer-D"),
    };
}

function setAnswerHandlers() {
    for (let ans in answers) {
        answers[ans].button.addEventListener("click",
            async () => await checkAnswer(ans));
    }
}

function setHandlers() {
    const buttons = document.getElementsByClassName("main-btn");
    for (let btn of buttons) {
        let ansNum = btn.getElementsByClassName("answer-number")[0];
        let svg = btn.children[0]
            .contentDocument
            .getElementById("fill");
        btn.onmouseover = btn.onmouseout = btn.onfocus = btn.onblur =
            createOnMouseEventsHandler(btn, svg, ansNum);
    }
}

function createOnMouseEventsHandler(btn, svg, ansNum) {
    const colorByEvent = {
        mouseover: ORANGE,
        focus: ORANGE,
        mouseout: BLUE,
        blur: BLUE,
    }

    return async function(event) {
        if (btn.classList.contains("disabled")
            || (!event.type in colorByEvent)) {
            return;
        }
        svg.setAttribute("fill", colorByEvent[event.type]);
        if (ansNum !== undefined) {
            if (event.type === "mouseover" || event.type === "focus") {
                ansNum.classList.add("white-color");
            }
            if (event.type === "mouseout" || event.type === "blur") {
                ansNum.classList.remove("white-color");
            }
        }
    }
}