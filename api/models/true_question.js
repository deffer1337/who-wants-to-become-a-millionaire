const {DataTypes} = require('sequelize');
const sequelize = require('./connect');
const question = require('./question');


const true_question = sequelize.define(
    'true_question',
    {
        answer: {
            type: DataTypes.STRING,
            allowNull: false,
            validate:{
                isIn: [['A', 'B', 'C', 'D']]
            }
        }
    },
    {
        // Здесь определяются другие настройки модели
    }
);

question.hasOne(true_question, {onDelete: "cascade"});

module.exports = true_question;