const {getReqData} = require("./utils");
const {Sequelize} = require('sequelize');


class APIUserView {
    constructor(userService) {
        this.userService = userService
    }

    async get(request, response) {
        const users = await this.userService.getUsers();
        response.writeHead(200, {"Content-Type": "application/json"});
        response.end(JSON.stringify(users));
    }

    async put(request, response) {
        const user_id = request.url.split("/")[3];
        const user_data = await getReqData(request);
        try {
            const user = await this.userService.updateUser(user_id, user_data)
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(user))
        }
        catch (error) {
            if (error instanceof Sequelize.ValidationError) {
                response.writeHead(422, {"Content-Type": "application/json"});
                response.end(JSON.stringify({message: error.message}));
            }
        }
    }

    async get_for_id(request, response) {
        try {
            const user_id = request.url.split("/")[3];
            const user = await this.userService.getUser(user_id);
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(user));
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"});
            response.end(JSON.stringify({message: error.message}));
        }
    }

    async post(request, response) {
        try {
            const user_data = await getReqData(request);
            const user = await this.userService.createUser(JSON.parse(user_data));
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(user));
        }
        catch (error)
        {
            if (error instanceof Sequelize.ValidationError) {
                response.writeHead(422, {"Content-Type": "application/json"});
                response.end(JSON.stringify({message: error.message}));
            }
            else
            {
                response.writeHead(400, {"Content-Type": "application/json"})
                response.end(JSON.stringify({message: "Bad Request"}))
            }
        }

    }
}

module.exports = APIUserView;
