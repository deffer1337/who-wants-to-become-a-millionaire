const { Op } = require("sequelize");


class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository
    }

    async createUser(user) {
        const newUser = await this.userRepository.create(user)
        return newUser
    }

    async updateUser(user_id, user) {
        const result = await this.userRepository.update(JSON.parse(user), {where: {id: user_id}});
        return new Promise((resolve, reject) => {
            if (result[0]) {
                resolve({message: 'Success'})
            } else {
                reject({message: 'Not success'})
            }
        });
    }

    async getUsers() {
        return new Promise((resolve, _) => {
            const users = this.userRepository.findAll(
                {
                    where: {
                        points: {
                            [Op.ne]: 0
                        }
                    },
                    order: [['points', 'DESC']],
                },
            );
            resolve(users);
        });
    }

    async getUser(user_id) {
        return new Promise((resolve, reject) => {
            const user = this.userRepository.findOne({
                where: {
                    id: user_id,
                },
            });

            if (user) {
                resolve(user);
            } else {
                reject(`User with id ${user_id} not found`);
            }
        });
    }
}

module
    .exports = UserService;
