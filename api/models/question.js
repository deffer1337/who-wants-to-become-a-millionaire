const {DataTypes} = require('sequelize');
const sequelize = require('./connect');


const question = sequelize.define(
    'question',
    {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        A: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        B: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        C: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        D: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        complexity: {
            type: DataTypes.INTEGER,
            allowNull: false,
        }
    },
    {
        // Здесь определяются другие настройки модели
    }
);

module.exports = question;
