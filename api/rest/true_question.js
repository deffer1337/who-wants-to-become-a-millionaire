

class APITrueQuestionView {
    constructor(trueQuestionService) {
        this.trueQuesionService = trueQuestionService
    }

    async get(request, response) {
        try {
            const question_id = request.url.split("/")[3];
            const true_question = await this.trueQuesionService.getTrueQuestion(question_id);
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(true_question));
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"});
            response.end(JSON.stringify({message: error.message}));
        }
    }
}

module.exports = APITrueQuestionView;
