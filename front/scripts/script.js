let questionNumber = 1;
let questionId = -1;
const questionElem = document.getElementById('question');
let score = 0;
let username = "";
let userId = -1;

let checkAnswer = async function(answer) {
    const trueAnswer = (await fetch(
        `https://lielikeimajoke.backend22.2tapp.cc/million/api/true_questions/${questionId}`)
        .then(data => data.json()))["answer"];
    console.log(trueAnswer);
    clearIntervalCountdown();
    changeButtonColor(trueAnswer, "#1fda13");
    await stopHover(trueAnswer);
    if (trueAnswer !== answer) {
        changeButtonColor(answer, '#e29a2e');
        stopHover(answer);
        setTimeout(async () => {
            backHover(answer);
            backHover(trueAnswer);
            changeButtonColor(answer, '#070556');
            changeButtonColor(trueAnswer, '#070556');
            endGame(false);
        }, 1000);
    }
    else {
        score += 10 + timeToAnswerInSeconds - leftTimeInSeconds;
        setTimeout(async () => {
            backHover(trueAnswer);
            changeButtonColor(trueAnswer, '#070556');
            if (questionNumber === 15) {
                endGame(true);
                return;
            }
            questionNumber++;
            await getNewQuestion();
            setIntervalCountdown();
        }, 1000);
    }
}

function changeButtonColor(answer, colorBg) {
    answers[answer].imgFill.setAttribute("fill", colorBg);
}

async function getNewQuestion() {
    const question = await fetch(
        `https://lielikeimajoke.backend22.2tapp.cc/million/api/questions/${questionNumber}`)
        .then(data => data.json());
    questionId = question["id"];
    questionElem.innerHTML = question["name"];
    for (let ans in answers) {
        answers[ans].text.textContent = question[ans];
    }
}

function stopHover(answer) {
    answers[answer].button.classList.add("disabled");
}

function backHover(answer) {
    answers[answer].button.classList.remove("disabled");
}

async function fillLeaderBoard() {
    const leaderBoardList = document.getElementById("leaderboard-list");
    const users = await fetch(
        `https://lielikeimajoke.backend22.2tapp.cc/million/api/users`)
        .then(data => data.json());
    removeTbody(leaderBoardList);
    let counter = 0;
    let tbody = document.createElement("tbody");
    for (let user of users) {
        let tr = document.createElement("tr");
        let tdNum = document.createElement("td");
        tdNum.innerText = (++counter).toString();
        let tdNickname = document.createElement("td");
        tdNickname.innerText = user["username"];
        let tdWinning = document.createElement("td");
        tdWinning.innerText = user["points"];
        tr.append(tdNum, tdNickname, tdWinning);
        tbody.append(tr);
    }
    leaderBoardList.append(tbody);
}

function removeTbody(table) {
    const tbodyElems = table.getElementsByTagName("tbody");
    for (let tbody of tbodyElems) {
        tbody.remove();
    }
}

function hideAll() {
    let toHide = document.getElementsByClassName("screen");
    for (let elem of toHide) {
        elem.classList.add("hidden");
    }
}

function hideAllExcept(...elements) {
    hideAll();
    elements.forEach(e => e.classList.remove("hidden"));
}

function endGame(isWin) {
    stopCountdown();
    hideAllExcept(document.getElementById("block-elements"),
        document.getElementById("question_screen"));
    document.getElementById("message").textContent = isWin
        ? "Поздравляем! Вы выиграли" : "К сожалению, вы проиграли";
    document.getElementById("score").textContent = `Ваш счет: ${score}`;
}

async function updateUserScore() {
    if (userId === -1) {
        return;
    }
    const points = {
        'points': score
    };
    const options = {
        method: 'PUT', body: JSON.stringify(points),
        headers: {'Content-Type': 'application/json'}
    }
    await fetch(`https://lielikeimajoke.backend22.2tapp.cc/million/api/users/${userId}`, options);
}

document.getElementById("leaderboard-btn").addEventListener("click", async () => {
    await fillLeaderBoard();
    hideAllExcept(document.getElementById("leaderboard"));
});

async function startGame() {
    await updateUserScore();
    questionNumber = 1;
    questionId = -1;
    score = 0;
    await getNewQuestion();
    setIntervalCountdown();
    hideAllExcept(document.getElementById("question_screen"))
}

for (let el of document.getElementsByClassName("start-game")) {
    el.addEventListener("click", async () => {
       await startGame();
    });
}

for (let el of document.getElementsByClassName("back-to-menu")) {
    el.addEventListener("click", async () => {
        hideAllExcept(document.getElementById("menu"));
    });
}

document.getElementById("submit").addEventListener("click", async () => {
    const input = document.forms[0].elements[0].value;
    if (input === "") {
        await startGame();
        return;
    }
    const user = {
        username: input,
    }
    const options = {
        method: 'POST', body: JSON.stringify(user),
        headers: {'Content-Type': 'application/json'}
    }
    const response = await fetch('https://lielikeimajoke.backend22.2tapp.cc/million/api/users', options);
    if (!response.ok) {
        return;
    }

    username = input;
    userId = (await response.json())["id"];
    await startGame();
})






