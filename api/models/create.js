const question = require('./question');
const user = require('./user');
const true_question = require('./true_question');

// Создаем таблицу user в бд
user.sync({force: true}).then(() => {
    console.log("Table user have been created");
}).catch(err => console.log(err));

// Создаем таблицу question в бд
question.sync({force: true}).then(() => {
    console.log("Table question have been created");
}).catch(err => console.log(err));

// Создаем таблицу true_question в бд
true_question.sync({force: true}).then(() => {
    console.log("Table true_question have been created");
}).catch(err => console.log(err));
