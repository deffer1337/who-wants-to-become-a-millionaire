const UserModel = require('./user');
const QuestionModel = require('./question');
const TrueQuestionModel = require('./true_question');

module.exports = {
    UserRepo: UserModel,
    QuestionRepo: QuestionModel,
    TrueQuestionRepo: TrueQuestionModel,
};
