

class QuestionService {
    constructor(questionRepository) {
        this.questionRepository = questionRepository
    }

    async getQuestions(complexity) {
        return new Promise((resolve, _) => {
            resolve(this.questionRepository.findAll({
                where: {
                    complexity: complexity,
                },
            }));
        });
    }
}

module.exports = QuestionService;
