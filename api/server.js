const http = require('http');
const {APIUserView, APIQuestionView, APITrueQuestionView, NotFound} = require("./rest");
const {UserService, QuestionService, TrueQuestionService} = require("./services")
const {UserRepo, QuestionRepo, TrueQuestionRepo} = require("./models")


const PORT = process.env.PORT || 8000;


const userService = new UserService(UserRepo)
const questionService = new QuestionService(QuestionRepo)
const trueQuestionService = new TrueQuestionService(TrueQuestionRepo)
const apiUserView = new APIUserView(userService);
const apiQuestionView = new APIQuestionView(questionService);
const apiTrueQuestionView = new APITrueQuestionView(trueQuestionService);


const server = http.createServer(async (request, response) => {
        // /api/users : GET
        if (request.url === '/api/users' && request.method === 'GET') {
            await apiUserView.get(request, response);
        }

        // /api/users/user_id : GET
        else if (request.url.match(/\/api\/users\/([0-9]+)/) && request.method === "GET") {
            await apiUserView.get_for_id(request, response);
        }

        // /api/users/user_id : PUT
        else if (request.url.match(/\/api\/users\/([0-9]+)/) && request.method === "PUT") {
            await apiUserView.put(request, response);
        }

        // /api/users : POST
        else if (request.url === "/api/users" && request.method === "POST") {
            await apiUserView.post(request, response);
        }

        // /api/questions/complexity : GET
        else if (request.url.match(/\/api\/questions\/([0-9]+)/) && request.method === 'GET') {
            await apiQuestionView.get(request, response);
        }

        // /api/true_questions/question_id : GET
        else if (request.url.match(/\/api\/true_questions\/([0-9]+)/) && request.method === "GET") {
            await apiTrueQuestionView.get(request, response);
        }

        // Route not found
        else {
            await NotFound(request, response);
        }
    }
);


server.listen(PORT, () => {
    console.log(`server started on port: ${PORT}`);
});
