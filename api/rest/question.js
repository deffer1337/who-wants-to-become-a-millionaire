

class APIQuestionView
{
    constructor(questionService) {
        this.questionService = questionService
    }

    async get(request, response)
    {
        try {
            const complexity = request.url.split("/")[3];
            const questions = await this.questionService.getQuestions(complexity);
            const lengthQuestions = questions.length;
            const random = Math.floor(Math.random() * lengthQuestions - 1);
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(questions[random]));
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"});
            response.end(JSON.stringify({message: error.message}));
        }
    }
}

module.exports = APIQuestionView;
