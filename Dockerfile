FROM node:18

WORKDIR /who-wants-to-become-a-millionaire

COPY package*.json ./

RUN npm install

COPY . .
