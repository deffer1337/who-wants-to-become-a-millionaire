const timer = document.getElementById('timer');
const timerCircleLine = timer.children[0];
const timeToAnswerInSeconds = 30;
const countdownText = timer.children[0].children[1];
let leftTimeInSeconds = timeToAnswerInSeconds;
const circleLineScale = 360 / timeToAnswerInSeconds;
let interval = null;

async function updateCountdown() {
    countdownText.textContent = (leftTimeInSeconds).toString();
    timerCircleLine.style.background = `conic-gradient(
        #ad6e04 ${leftTimeInSeconds * circleLineScale}deg,
        #040c15 ${leftTimeInSeconds * circleLineScale}deg
    )`;
    if (leftTimeInSeconds === 0) {
        clearInterval(interval)
        endGame(false);
    }
    leftTimeInSeconds--;
}

function stopCountdown() {
    leftTimeInSeconds = timeToAnswerInSeconds;
}

function clearIntervalCountdown() {
    clearInterval(interval);
}

function setIntervalCountdown() {
    clearInterval(interval);
    leftTimeInSeconds = timeToAnswerInSeconds;
    interval = setInterval(updateCountdown, 1000);
}