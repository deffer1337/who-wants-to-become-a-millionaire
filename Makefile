build:
	docker-compose build

push:
	docker-compose push

pull:
	docker-compose pull

up:
	docker-compose up -d

down:
	docker-compose down