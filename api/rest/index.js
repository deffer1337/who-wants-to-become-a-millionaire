const APIUserView = require('./user');
const APIQuestionView = require('./question');
const APITrueQuestionView = require('./true_question');
const NotFound = require('./not_found');


module.exports = {
    APIUserView: APIUserView,
    APIQuestionView: APIQuestionView,
    APITrueQuestionView : APITrueQuestionView,
    NotFound: NotFound,
};
